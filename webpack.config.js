const webpack = require('webpack');
const path = require('path');

const config = {
    mode: 'development',
    entry: path.resolve(__dirname, 'src') + '/app/index.js', /*?*/
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist') + '/app/', /*?*/
        publicPath: '/', /*?*/
    },
    devServer: {
        contentBase: './src/',
        hot: true
    },
    module: {
        rules: [
            { 
                test: /\.js$/, 
                exclude: /node_modules/, 
                loader: "babel-loader" 
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};

module.exports = config;