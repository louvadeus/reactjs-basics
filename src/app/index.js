import React from 'react';
import ReactDOM from 'react-dom';

import Header from './components/Header';
import Home from './components/Home';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            homeLink: 'Home',
            homeMounted: true
        }
    }

    onGreet = () => {
        alert('Hello!');
    }

    onChangeLinkName = (newName) => {
        this.setState({
            homeLink: newName
        })
    }

    onChangeHomeMounted(){
        this.setState({
            homeMounted: !this.state.homeMounted
        })
    }

    render(){

        let homeCmp = this.state.homeMounted 
            ? (
                <Home 
                    name={'Joao'}
                    age={38}
                    greet={this.onGreet}
                    changeLink={this.onChangeLinkName}
                    initialLinkName={this.state.homeLink}
                />
            )
            : '';


        return (
            <div>
                <Header
                    homeLink={this.state.homeLink}
                />
                {homeCmp}
                <hr/>
                <button onClick={this.onChangeHomeMounted.bind(this)}>
                    (Un)Mount Home component
                </button>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'));


/*
componentWillMount -> immediatly before initial rendering

componentDidMount -> immediatly after initial rendering

componentWillRecieveProps -> when component receives new props

shouldComponentUpdate -> before rendering, after receiving new props or state

-> return false to prevent rendering!

componentWillUpdate -> before rendering, after receiving new props or state

componentDidUpdate -> after component's updates are flushed to DOM

componentWillUnmount -> immediatly before removing component from DOM

*/