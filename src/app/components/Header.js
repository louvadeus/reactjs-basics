import React from 'react';

class Header extends React.Component {
    
    render() {
        return (
            <div>
                <ul>
                    <li><a href='#'>{this.props.homeLink}</a></li>
                </ul>
            </div>
            
        )
    }
}

export default Header;