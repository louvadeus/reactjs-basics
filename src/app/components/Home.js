import React from 'react';

class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            age: props.age,
            status: 0,
            homeLink: props.initialLinkName
        }
        setTimeout(()=>{
            this.setState({status: 1})
        }, 3000);
        console.log('[Constructor]');
    }

    componentWillMount(){
        console.log('[Component will mount]');
        // if we change the state in this method, this new state will be taking into account. Only one render
    }

    componentDidMount(){
        console.log('[Component did mount]');
    }

    componentWillReceiveProps(nextProps){
        console.log('[Component will receive props]', nextProps)
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('[Should component update]',  nextProps, nextState);
        // if(nextState.status === 1) {
        //     return false;
        // }
        return true;
    }

    componentWillUpdate(nextProps, nextState){
        console.log('[Component will update]', nextProps, nextState)
    }

    componentDidUpdate(prevProps, prevState){
        console.log('[Component did update]', prevProps, prevState)
    }

    componentWillUnmount(){
        console.log('[Component will unmount]');
    }

    onMakeOlder(){
        this.setState({
            age: this.state.age + 3
        })
    }

    onChangeLink = () => {
        this.props.changeLink(this.state.homeLink)
    }

    onHandleChange = (e) => {
        this.setState({homeLink: e.target.value})
    }

    render() {
        return (
            <div>
                <p>In a new Component!</p>
                <p>Your name is {this.props.name}, your age is {this.state.age}</p>
                <p>Status: {this.state.status}</p>
                <hr></hr>
                <button onClick={this.onMakeOlder.bind(this)}>Make me older!</button>
                <hr/>
                <button onClick={this.props.greet}>Greet</button>
                <hr/>
                <input
                    type='text'
                    value={this.state.homeLink}
                    onChange={this.onHandleChange}
                />
                <button onClick={this.onChangeLink}>Change Header Link</button>
            </div>
        )
    }
}

export default Home;